﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using SnelTransportAPI.Controllers;
using SnelTransportAPI.Models;

namespace API_UnitTests
{
    [TestClass]
    public class ArtikelTypeTests
    {
        [TestMethod]
        public void GetAllArtikels_ShouldGetAllArtikels()
        {
            List<Artikel> testArtikelen = GetEmptyTestArtikelen();

            ArtikelsController controller = new ArtikelsController();
            var qresult = controller.GetArtikels();
            
            var result = qresult.ToList();

            Assert.AreEqual(testArtikelen.Count, result.Count);
        }

        [TestMethod]
        public void PostOneArtikel_ShouldReturnPostedArtikel()
        {
            Artikel testArtikel = new Artikel()
            {
                ArtikelNummer = "RE23",
                Naam = "TestNaam",
                Prijs = 23.50m,
                Warenhuis = "WHB",
                Vak = 2,
                TypeID = 1
            };
            ArtikelsController controller = new ArtikelsController();

            var result = controller.PostArtikel(testArtikel);

            Assert.AreEqual(testArtikel, result.Result);
        }

        private List<Artikel> GetEmptyTestArtikelen()
        {
            List<Artikel> artikelen = new List<Artikel>();
            for (int i = 0; i < 66; i++)
            {
                artikelen.Add(new Artikel());
            }
            return artikelen;
        }
    }
}
