﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SnelTransportAPI.Models
{
    public class Artikel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ArtikelNummer { get; set; }
        [Required]
        public string Naam { get; set; }
        public decimal Prijs { get; set; }
        public string Warenhuis { get; set; }
        public int Vak { get; set; }

        public int TypeID { get; set; }
        public ArtikelType Type { get; set; }
    }
}