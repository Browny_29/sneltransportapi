namespace SnelTransportAPI.Migrations
{
    using SnelTransportAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SnelTransportAPI.Models.SnelTransportAPIPOSTGRESContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SnelTransportAPI.Models.SnelTransportAPIPOSTGRESContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.ArtikelTypes.AddOrUpdate(x => x.Id,
                new ArtikelType() { Type = "Keyboards" },
                new ArtikelType() { Type = "Mouse" },
                new ArtikelType() { Type = "Displays" },
                new ArtikelType() { Type = "Board" },
                new ArtikelType() { Type = "Network" },
                new ArtikelType() { Type = "Storage" },
                new ArtikelType() { Type = "Tablets" },
                new ArtikelType() { Type = "Laptops" },
                new ArtikelType() { Type = "Computers" }
            );

            context.Artikels.AddOrUpdate(x => x.Id,
                new Artikel()
                {
                    ArtikelNummer = "90-XB1000KM00020",
                    Naam = "Asus U2000 Keyboard + Mouse Set",
                    Prijs = 16.45m,
                    Warenhuis = "WHB",
                    Vak = 1,
                    TypeID = 1
                },
                new Artikel()
                {
                    ArtikelNummer = "90-XB2400KM00130-",
                    Naam = "Asus W3000 Desktop - Draadloos - Wit - Qwerty",
                    Prijs = 24.10m,
                    Warenhuis = "WHB",
                    Vak = 2,
                    TypeID = 1
                });
        }
    }
}
