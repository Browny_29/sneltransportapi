﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SnelTransportAPI.Models;

namespace SnelTransportAPI.Controllers
{
    public class ArtikelTypesController : ApiController
    {
        private SnelTransportAPIPOSTGRESContext db = new SnelTransportAPIPOSTGRESContext();

        // GET: api/ArtikelTypes
        public IQueryable<ArtikelType> GetArtikelTypes()
        {
            return db.ArtikelTypes;
        }

        // GET: api/ArtikelTypes/5
        [ResponseType(typeof(ArtikelType))]
        public async Task<IHttpActionResult> GetArtikelType(int id)
        {
            ArtikelType artikelType = await db.ArtikelTypes.FindAsync(id);
            if (artikelType == null)
            {
                return NotFound();
            }

            return Ok(artikelType);
        }

        // PUT: api/ArtikelTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikelType(int id, ArtikelType artikelType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikelType.Id)
            {
                return BadRequest();
            }

            db.Entry(artikelType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikelTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ArtikelTypes
        [ResponseType(typeof(ArtikelType))]
        public async Task<IHttpActionResult> PostArtikelType(ArtikelType artikelType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ArtikelTypes.Add(artikelType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = artikelType.Id }, artikelType);
        }

        // DELETE: api/ArtikelTypes/5
        [ResponseType(typeof(ArtikelType))]
        public async Task<IHttpActionResult> DeleteArtikelType(int id)
        {
            ArtikelType artikelType = await db.ArtikelTypes.FindAsync(id);
            if (artikelType == null)
            {
                return NotFound();
            }

            db.ArtikelTypes.Remove(artikelType);
            await db.SaveChangesAsync();

            return Ok(artikelType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelTypeExists(int id)
        {
            return db.ArtikelTypes.Count(e => e.Id == id) > 0;
        }
    }
}