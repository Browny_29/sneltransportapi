﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SnelTransportAPI.Models;

namespace SnelTransportAPI.Controllers
{
    public class ArtikelsController : ApiController
    {
        private SnelTransportAPIPOSTGRESContext db = new SnelTransportAPIPOSTGRESContext();

        // GET: api/Artikels
        public IQueryable<Artikel> GetArtikels()
        {
            return db.Artikels;
        }

        // GET: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> GetArtikel(int id)
        {
            Artikel artikel = await db.Artikels.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        // PUT: api/Artikels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikel(int id, Artikel artikel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikel.Id)
            {
                return BadRequest();
            }

            db.Entry(artikel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artikels
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> PostArtikel(Artikel artikel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Artikels.Add(artikel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = artikel.Id }, artikel);
        }

        // DELETE: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> DeleteArtikel(int id)
        {
            Artikel artikel = await db.Artikels.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            db.Artikels.Remove(artikel);
            await db.SaveChangesAsync();

            return Ok(artikel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelExists(int id)
        {
            return db.Artikels.Count(e => e.Id == id) > 0;
        }
    }
}